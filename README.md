# Tester son code

## Problématique

Les applications grossissent quasi naturellement, il devient difficile de
s'assurer de la justesse du code quand on refactorise.

On veut aussi pouvoir démontrer la justesse du code par rapport à une norme. Et
vérifier automatiquement que c'est toujours le cas.

Et enfin, on remarque bien souvent que le code qui est testé est de meilleur
qualité, que les tests forment une sorte de documentation.

## Le scénario de test

On va tester un ensemble de fonctions mathématiques:

    - factorielle
    - plus grand commun dénominateur

## Recherches

Mots clés: *unit testing python*

-> https://docs.python.org/3/library/unittest.html
-> https://docs.python-guide.org/writing/tests/

### unittest

Toujours là avec python
Assez simple

## doctest

Toujours là avec python
Documente le code
Configuration difficile
Assez fragile

Le mélange des genres peut rendre les choses moins lisibles

### pytest

Découverte des tests automatique (peut aussi lancer les unittests mais pas les
doctests)
Utilisation super simple à base de *assert*
Remplace les méthodes *setUp* et *tearDown* par un concept de fixture assez
puissant
Output un poil plus court et donc sympa

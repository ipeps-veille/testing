import pytest
from mathfunc import factorial, gcd, NotANumberError


def test_factorial():
    assert factorial(0) == 1
    assert factorial(1) == 1
    assert factorial(4) == 24

    with pytest.raises(NotANumberError):
        factorial('toto')

    with pytest.raises(ValueError):
        factorial(-1)

import unittest
from mathfunc import factorial, gcd, NotANumberError


class MathTestCase(unittest.TestCase):

    def test_factorial(self):
        self.assertEqual(factorial(0), 1)
        self.assertEqual(factorial(1), 1)
        self.assertEqual(factorial(4), 24)
        self.assertEqual(factorial(6), 720)

        with self.assertRaises(NotANumberError):
            factorial('toto')

        with self.assertRaises(ValueError):
            factorial(-1)


if __name__ == '__main__':
    import doctest
    doctest.testfile('mathfunc.py', globs=globals())
    unittest.main()

class NotANumberError(BaseException):
    pass


def factorial(x):
    try:
        if x < 0:
            raise ValueError(x)
        result = 1
        while x > 0:
            result *= x
            x -= 1
        return result
    except TypeError:
        raise NotANumberError(x)


def gcd(a, b):
    """Return the greatest common divisor between a and b
    >>> gcd(12, 9)
    3
    >>> gcd(9, 12)
    3
    >>> gcd(8, -6)
    2
    >>> results = []
    >>> for x in range(1, 10):
    ...     results.append(gcd(x, 1))
    >>> results == [1] * 9
    True
    >>> try:
    ...     gcd('a', 3)
    ... except NotANumberError:
    ...     'OK'
    'OK'

    """
    if b == 0:
        return a
    try:
        return abs(gcd(b, a % b))
    except TypeError:
        raise NotANumberError